import java.util.HashMap;
import java.util.Map;

public class App {
    public HashMap<String, Day> datesHashMap = new HashMap<>();

    public void addTask(String date, String hr, Task task){
        Day d = datesHashMap.get(date);

        if (d != null){
            d.addTasker(hr, task);
        }else{
            Day day = new Day();
            day.date = date;
            day.addTasker("5:00", task);

            datesHashMap.put(day.date, day);
        }
    }

    public void showTask(){
        for (Map.Entry<String, Day> entry : datesHashMap.entrySet()){
            Day day = entry.getValue();
            System.out.println("Day: " + day.date);
            System.out.println("--------------------");
            System.out.println("Task: ");
            for (Map.Entry<String, Task> o : day.taskHashMap.entrySet()){
                System.out.println("-->" + o.getKey() + " " + o.getValue().taskName);
            }
            System.out.println("--------------------");
        }
    }
}
