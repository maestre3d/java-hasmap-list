public class Test_App {
    public Test_App(){
        App app = new App();

        Task t = new Task();
        t.taskName = "Go to gym";
        app.addTask("20/09/2018", "5:00", t);

        Task t2 = new Task();
        t2.taskName = "Go with friends";
        app.addTask("21/09/2018", "7:00", t2);

        app.showTask();
    }
}
